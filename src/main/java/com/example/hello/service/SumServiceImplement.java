package com.example.hello.service;

import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SumServiceImplement implements SumService{

    @Override
    public int sum(List<Integer> soNguyen) {
        int s = 0;
        for (int i = 0; i< soNguyen.size(); i++)
            s= s+ soNguyen.get(i);
        return s;
    }
}
