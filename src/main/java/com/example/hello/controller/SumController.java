package com.example.hello.controller;

import com.example.hello.service.SumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "")
public class SumController {
    @Autowired
    public SumService sumService;

    @PostMapping("/api")
    public Integer add(@RequestBody SumListRequest sum) {
        int kq = sumService.sum(sum.getSumList());
        return kq;
    }
}
